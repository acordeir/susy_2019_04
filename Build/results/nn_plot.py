import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter
from numpy import genfromtxt
my_data = genfromtxt('nn_6j_output.csv', delimiter=',')
bin_list=[0.0, 0.5194444444444445, 0.6888888888888889, 0.8138888888888889, 1.0]
my_data_1=[]
my_data_2=[]
my_data_3=[]
my_data_4=[]
for i in range(1,len(bin_list)):
    print(bin_list[i]-bin_list[i-1])
##    if i < bin_list[1]:
#        my_data_1.append(i)
#    elif i < bin_list[2]:
#        my_data_2.append(i) 


plt.hist(my_data, bins=200)
plt.title("6-jet NN on benchmark Higgsino 300 GeV")
plt.xlabel("NN Bins")
plt.ylabel("Events")
#plt.gca().yaxis.set_major_formatter(PercentFormatter(1))
plt.show()
