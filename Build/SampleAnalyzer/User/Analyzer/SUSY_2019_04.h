#ifndef ANALYSIS_SUSY_2019_04_H
#define ANALYSIS_SUSY_2019_04_H

#include "SampleAnalyzer/Process/Analyzer/AnalyzerBase.h"

namespace MA5
{
    class SUSY_2019_04 : public AnalyzerBase
    {
        INIT_ANALYSIS(SUSY_2019_04, "SUSY_2019_04")

        public:
            bool Initialize(const MA5::Configuration& cfg,
                            const std::map<std::string,std::string>& parameters);
            void Finalize(const SampleFormat& summary, const std::vector<SampleFormat>& files);
            bool Execute(SampleFormat& sample, const EventFormat& event);

        private:
    };
}

#endif // ANALYSIS_SUSY-2019-04_H