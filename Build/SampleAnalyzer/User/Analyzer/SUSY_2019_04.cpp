#include "SampleAnalyzer/User/Analyzer/SUSY_2019_04.h"
#include <onnxruntime_cxx_api.h>
#include <random>

using namespace MA5;
using namespace std;

std::knuth_b rand_engine;  // replace knuth_b with one of the engines listed below
std::uniform_real_distribution<> uniform_zero_to_one(0.0, 1.0);


bool random_bool_with_prob( double prob )  // probability between 0.0 and 1.0
{
    return uniform_zero_to_one(rand_engine) < prob;
}

// Overlap Removal
template<typename T1, typename T2> std::vector<const T1*>
  Removal(std::vector<const T1*> &v1, std::vector<const T2*> &v2,
  const double &drmin)
{
  // Determining with objects should be removed
  std::vector<bool> mask(v1.size(),false);
  for (unsigned int j=0;j<v1.size();j++)
    for (unsigned int i=0;i<v2.size();i++)
      if (v2[i]->dr(v1[j]) < drmin)
      {
        mask[j]=true;
        break;
      }

  // Building the cleaned container
  std::vector<const T1*> cleaned_v1;
  for (unsigned int i=0;i<v1.size();i++)
    if (!mask[i]) cleaned_v1.push_back(v1[i]);

  return cleaned_v1;
}

// Overlap Removal with spedial Delta R 
template<typename T1, typename T2> std::vector<const T1*>
  Removal_last(std::vector<const T1*> &v1, std::vector<const T2*> &v2)
{
  // Determining with objects should be removed
  std::vector<bool> mask(v1.size(),false);
  for (unsigned int j=0;j<v1.size();j++){
    const double drmin = std::min(0.4,0.04 + (10 / v1[j]->pt()));
    for (unsigned int i=0;i<v2.size();i++)
      if (v2[i]->dr(v1[j]) < drmin)
      {
        mask[j]=true;
        break;
      }
  }
  // Building the cleaned container
  std::vector<const T1*> cleaned_v1;
  for (unsigned int i=0;i<v1.size();i++)
    if (!mask[i]) cleaned_v1.push_back(v1[i]);

  return cleaned_v1;
}


// Neural network paths
std::string nn_path_4j = "/Users/acordeir/Documents/LPNHE/ATLAS/phenomenology/MA5/ONNX-FILES/SUSY-2019-04-ONNX/SUSY-2019-04_4jets.onnx";
std::string nn_path_5j = "/Users/acordeir/Documents/LPNHE/ATLAS/phenomenology/MA5/ONNX-FILES/SUSY-2019-04-ONNX/SUSY-2019-04_5jets.onnx";
std::string nn_path_6j = "/Users/acordeir/Documents/LPNHE/ATLAS/phenomenology/MA5/ONNX-FILES/SUSY-2019-04-ONNX/SUSY-2019-04_6jets.onnx";
std::string nn_path_7j = "/Users/acordeir/Documents/LPNHE/ATLAS/phenomenology/MA5/ONNX-FILES/SUSY-2019-04-ONNX/SUSY-2019-04_7jets.onnx";
std::string nn_path_8j = "/Users/acordeir/Documents/LPNHE/ATLAS/phenomenology/MA5/ONNX-FILES/SUSY-2019-04-ONNX/SUSY-2019-04_8jets.onnx";
std::vector<float> nn_6j_output; 
MAuint32 i_event =0;
MAuint32 i_event_old =0;



void write_data(std::vector<float> data, std::string fname){
  std::ofstream outputFile(fname); 
  if (outputFile.is_open()) {
    if (data.size()>1){
    for (MAuint32 i=0; i < data.size(); i++){
    outputFile << data[i]; 
    if (i<data.size()-1){
      outputFile << ","; 
    }
    }
    }
   outputFile.close(); 
   std::cout << "Data was written to " << fname << endl;
   }
  else {
    std::cerr << "Error opening file\n";
  }
}


static float run_ONNX(std::string modelPath, std::vector<float> input){
  Ort::Env env;
	Ort::RunOptions runOptions;
	Ort::Session session(nullptr);

  // create session
  session = Ort::Session(env, modelPath.c_str(), Ort::SessionOptions{ nullptr });
  
  // define names
  Ort::AllocatorWithDefaultOptions ort_alloc;

  Ort::AllocatedStringPtr inputName = session.GetInputNameAllocated(0, ort_alloc);  
  Ort::AllocatedStringPtr outputName = session.GetOutputNameAllocated(0, ort_alloc);

  const std::array<const char*,1> inputNames = {inputName.get()};
  const std::array<const char*,1> outputNames = {outputName.get()};

  // define shape
  const std::array<int64_t, 2> inputShape = {1,65};
  const std::array<int64_t, 2> outputShape = { 1,1 };
  auto input_tensor_size = 65;

  // define tensors 
  std::array<float, 1> results;
  
  auto memory_info = Ort::MemoryInfo::CreateCpu(OrtDeviceAllocator, OrtMemTypeCPU);

  auto inputTensor = Ort::Value::CreateTensor<float>(memory_info, input.data(), input_tensor_size, inputShape.data(), inputShape.size());
  auto outputTensor = Ort::Value::CreateTensor<float>(memory_info, results.data(), results.size(), outputShape.data(), outputShape.size());

  // run inference
  session.Run(runOptions, inputNames.data(), &inputTensor, 1, outputNames.data(), &outputTensor, 1);
  try {
    session.Run(runOptions, inputNames.data(), &inputTensor, 1, outputNames.data(), &outputTensor, 1);
  }
  catch (Ort::Exception& e) {
    std::cout << e.what() << std::endl;
    return 1;
  }

return results[0];

}
static int bjet_category(size_t num_bjets)
{
	switch (num_bjets) {
	case 0: return -1;
	case 1: return 0;
	case 2: return 1;
	case 3: return 2;
	case 4: return 3;
	default: return 3; // cat 3: >= 4 b-jets
	}
}

static int jet_btag_category(const RecJetFormat* jet)//, vector<int> jets_score_b, vector<int> jets_score_non_b)
{
	if (jet->btag()) {
    float rand = uniform_zero_to_one(rand_engine);
    if (rand<0.6){
      return 5;
    }
    else if (rand<0.7){
      return 4;
    }
    else if (rand<0.77){
      return 3;
    }
    else if (rand<0.85){
      return 2;
    }
    else{
      return 1;
    }
	}
  else{
    return 1;
  }

}

static float calc_three_jet_max_pt_mass(std::vector<const RecJetFormat*> jets,
                                        const MALorentzVector* lep=nullptr,
                                        const MALorentzVector* met=nullptr) 
{
  // adding 4-vectors
	// either only jets, or include both lepton and MET // lepton and met to be added 
  if (lep || met) {
		assert(lep != nullptr);
		assert(met != nullptr);
	}

	 MALorentzVector max_pt_system;

	for (size_t i=0; i < jets.size(); ++i) {
		for (size_t j=i+1; j < jets.size(); ++j) {
			for (size_t k=j+1; k < jets.size(); ++k) {

				MALorentzVector system = jets[i]->momentum() + jets[j]->momentum() + jets[k]->momentum();

        if (lep && met) {
					system += *lep;
					system += *met;
				}

				if (system.Pt() > max_pt_system.Pt()) {
					max_pt_system = system;
				}
			}
		}
	}
	return max_pt_system.M();
}

static float calc_ht(std::vector<const RecJetFormat*> jets) // should input be vector ?
{
	float ht = 0;
	for (size_t j=0; j < jets.size(); ++j) {
		ht += jets[j]->pt();
	}
	return ht;
}
static float min_dr_lep_jet(std::vector<const MALorentzVector*> leptons, std::vector<const RecJetFormat*> jets)
{
	float min_dr = 99; //init 
	for (size_t i=0; i < leptons.size(); ++i) {
		for (size_t j=0; j < jets.size(); ++j) {
      MALorentzVector jet_vec = jets[j]->momentum();
			const float dr = leptons[i]->DeltaR(jet_vec);
			if (dr < min_dr) {min_dr = dr;}
		}
	}
	return min_dr;
}

/* Function to get no of set bits in binary
 * representation of positive integer n */
static unsigned int countSetBits(unsigned int n)
{
	unsigned int count = 0;
	while (n)
	{
	    count += n & 1;
	    n >>= 1;
	}
	return count;
}

static float calc_minmax_mass(std::vector<const RecJetFormat*> jets, int jetdiff=10)
{
    const int nJets = jets.size();

    //bitwise representation of all jets and for which half they are selected
    // One bit for every jet, marking into which set they are grouped
    const unsigned int bitmax = 1 << nJets;
    float minmass = 999999999;

    for(unsigned int bit=0; bit < bitmax; bit++){
        const int bitcount = countSetBits(bit);
        if (abs(nJets - 2*bitcount) > jetdiff) {
        	continue;
        }

        MALorentzVector sum1, sum2;
        // loop through jets and assign to either sum, depending on bit
        for(int i=0; i<nJets; i++) {
            if (bit & (1<<i)) 
            	sum1 += jets[i]->momentum();
            else
            	sum2 += jets[i]->momentum();
        }
        if (sum1.M() > sum2.M() && sum1.M() < minmass) 
        	minmass = sum1.M();
        else if (sum2.M() > sum1.M() && sum2.M() < minmass)
        	minmass = sum2.M();
    }

    return minmass;
}


// -----------------------------------------------------------------------------
// Initialize
// function called one time at the beginning of the analysis
// -----------------------------------------------------------------------------
bool SUSY_2019_04::Initialize(const MA5::Configuration& cfg, const std::map<std::string,std::string>& parameters)
{
  cout << "BEGIN Initialization" << endl;   
   
  // Initializing PhysicsService for MC
  PHYSICS->mcConfig().Reset();

 
  // Initializing PhysicsService for RECO
  PHYSICS->recConfig().Reset();  


  // ========================= //
  // ===== Signal region ===== //
  // ========================= //
 
  Manager()->AddRegionSelection("preSelection");
  Manager()->AddRegionSelection("oneLeptonChannel");
  Manager()->AddRegionSelection("sameSignChannel");

  Manager()->AddRegionSelection("1l_15j20_0b");
  Manager()->AddRegionSelection("1l_15j20_3b");
  Manager()->AddRegionSelection("ss_10j20_0b");
  Manager()->AddRegionSelection("ss_10j20_3b");

  Manager()->AddRegionSelection("1l_12j40_0b");
  Manager()->AddRegionSelection("1l_12j40_3b");
  Manager()->AddRegionSelection("ss_8j40_0b");
  Manager()->AddRegionSelection("ss_8j40_3b");

  Manager()->AddRegionSelection("1l_12j60_0b");
  Manager()->AddRegionSelection("1l_12j60_3b");
  Manager()->AddRegionSelection("ss_7j60_0b");
  Manager()->AddRegionSelection("ss_7j60_3b");

  Manager()->AddRegionSelection("1l_10j80_0b");
  Manager()->AddRegionSelection("1l_10j80_3b");
  Manager()->AddRegionSelection("ss_6j80_0b");
  Manager()->AddRegionSelection("ss_6j80_3b");

  Manager()->AddRegionSelection("1l_8j100_0b");
  Manager()->AddRegionSelection("1l_8j100_3b");
  Manager()->AddRegionSelection("ss_6j100_0b");
  Manager()->AddRegionSelection("ss_6j100_3b");

// discovery regions for shape analysis
  Manager()->AddRegionSelection("ss_shape_6j_3b");

  Manager()->AddRegionSelection("1l_shape_4j_4b");
  Manager()->AddRegionSelection("1l_shape_5j_4b");
  Manager()->AddRegionSelection("1l_shape_6j_4b");
  Manager()->AddRegionSelection("1l_shape_7j_4b");
  Manager()->AddRegionSelection("1l_shape_8j_4b");

//std::string AllSR[] ={"1l_15j20_0b","1l_15j20_3b","1l_12j40_0b","1l_12j40_3b","1l_12j60_0b","1l_12j60_3b","1l_10j80_0b","1l_10j80_3b","1l_8j100_0b","1l_8j100_3b","1l_shape_4j_4b","1l_shape_5j_4b","1l_shape_6j_4b","1l_shape_7j_4b","1l_shape_8j_4b"};
std::string OneLeptonChannel[]={"1l_15j20_0b","1l_15j20_3b","1l_12j40_0b","1l_12j40_3b","1l_12j60_0b","1l_12j60_3b","1l_10j80_0b","1l_10j80_3b","1l_8j100_0b","1l_8j100_3b","1l_shape_4j_4b","1l_shape_5j_4b","1l_shape_6j_4b","1l_shape_7j_4b","1l_shape_8j_4b"};
std::string SameSignChannel[]={"ss_10j20_0b","ss_10j20_3b", "ss_8j40_0b","ss_8j40_3b","ss_7j60_0b","ss_7j60_3b","ss_6j80_0b","ss_6j80_3b","ss_6j100_0b","ss_6j100_3b","ss_shape_6j_3b"};
std::string ALLSR[] = {"1l_15j20_0b","1l_15j20_3b","1l_12j40_0b","1l_12j40_3b","1l_12j60_0b","1l_12j60_3b","1l_10j80_0b","1l_10j80_3b","1l_8j100_0b","1l_8j100_3b","1l_shape_4j_4b","1l_shape_5j_4b","1l_shape_6j_4b","1l_shape_7j_4b","1l_shape_8j_4b", "ss_10j20_0b","ss_10j20_3b", "ss_8j40_0b","ss_8j40_3b","ss_7j60_0b","ss_7j60_3b","ss_6j80_0b","ss_6j80_3b","ss_6j100_0b","ss_6j100_3b","ss_shape_6j_3b"};
std::string threeb[]={"1l_15j20_3b","1l_12j40_3b","1l_12j60_3b","1l_10j80_3b","1l_8j100_3b","ss_10j20_3b","ss_8j40_3b","ss_7j60_3b","ss_6j80_3b","ss_6j100_3b","ss_shape_6j_3b"};
std::string j_15_20[]={"1l_15j20_0b", "1l_15j20_3b"};
std::string j_12_40[]={"1l_12j40_0b", "1l_12j40_3b"};
std::string j_12_60[]={"1l_12j60_0b", "1l_12j60_3b"};
std::string j_10_80[]={"1l_10j80_0b", "1l_10j80_3b"};
std::string j_8_100[]={"1l_8j100_0b", "1l_8j100_3b"};


std::string NN_SS[] = {"1l_shape_4j_4b","1l_shape_5j_4b","1l_shape_6j_4b","1l_shape_7j_4b","1l_shape_8j_4b"};
 
 // ====================== //
  // ===== Selections ===== //
  // ====================== //
  
 Manager()->AddCut("presel nl>1", ALLSR);
 Manager()->AddCut("presel nl>1 ; lead pt > 27 GeV", ALLSR);


 Manager()->AddCut("presel njets > 4 ; > 20 GeV",    ALLSR);

 //Manager()->AddCut("n_bjets >= 3 ",                  threeb);
 Manager()->AddCut("one_lepton_cut",                 OneLeptonChannel);
 Manager()->AddCut("sameSignChannel",                SameSignChannel);

 //Number of jets 
 Manager()->AddCut("15 jets > 20 Gev", j_15_20);
 Manager()->AddCut("12 jets > 40 Gev", j_12_40);
 Manager()->AddCut("12 jets > 60 Gev", j_12_60);
 Manager()->AddCut("10 jets > 80 Gev", j_10_80);
 Manager()->AddCut("8 jets > 100 Gev", j_8_100);



 // Final cut on jet counting 
 // one lepton
 Manager()->AddCut("1l_15j20_0b_cut",                "1l_15j20_0b");
 Manager()->AddCut("1l_15j20_3b_cut",                "1l_15j20_3b");

 Manager()->AddCut("1l_12j40_0b_cut",                "1l_12j40_0b");
 Manager()->AddCut("1l_12j40_3b_cut",                "1l_12j40_3b");

 Manager()->AddCut("1l_12j60_0b_cut",                "1l_12j60_0b");
 Manager()->AddCut("1l_12j60_3b_cut",                "1l_12j60_3b");

 Manager()->AddCut("1l_10j80_0b_cut",                "1l_10j80_0b");
 Manager()->AddCut("1l_10j80_3b_cut",                "1l_10j80_3b");

 Manager()->AddCut("1l_8j100_0b_cut",                "1l_8j100_0b");
 Manager()->AddCut("1l_8j100_3b_cut",                "1l_8j100_3b");

 //same sign 
 Manager()->AddCut("ss_10j20_0b_cut",                "ss_10j20_0b");
 Manager()->AddCut("ss_10j20_3b_cut",                "ss_10j20_3b");

 Manager()->AddCut("ss_8j40_0b_cut",                  "ss_8j40_0b");
 Manager()->AddCut("ss_8j40_3b_cut",                  "ss_8j40_3b");

 Manager()->AddCut("ss_7j60_0b_cut",                  "ss_7j60_0b");
 Manager()->AddCut("ss_7j60_3b_cut",                  "ss_7j60_3b");

 Manager()->AddCut("ss_6j80_0b_cut",                  "ss_6j80_0b");
 Manager()->AddCut("ss_6j80_3b_cut",                  "ss_6j80_3b");

 Manager()->AddCut("ss_6j100_0b_cut",                  "ss_6j100_0b");
 Manager()->AddCut("ss_6j100_3b_cut",                  "ss_6j100_3b");


//Signal shape

 Manager()->AddCut("B-jets >= 4 (>20 GeV)",     NN_SS);

 Manager()->AddCut("1l_shape_4j_4b_cut",             "1l_shape_4j_4b");
 Manager()->AddCut("1l_shape_5j_4b_cut",             "1l_shape_5j_4b");
 Manager()->AddCut("1l_shape_6j_4b_cut",             "1l_shape_6j_4b");
 Manager()->AddCut("1l_shape_7j_4b_cut",             "1l_shape_7j_4b");
 Manager()->AddCut("1l_shape_8j_4b_cut",             "1l_shape_8j_4b");


 // ====================== //
// ===== Histograms ===== //
// ====================== //

  Manager()->AddHisto("4jets", 4, 0, 1., "1l_shape_4j_4b");
  Manager()->AddHisto("5jets", 4, 0, 1., "1l_shape_5j_4b");
  Manager()->AddHisto("6jets", 4, 0, 1., "1l_shape_6j_4b");
  Manager()->AddHisto("7jets", 4, 0, 1., "1l_shape_7j_4b");
  Manager()->AddHisto("8jets", 4, 0, 1., "1l_shape_8j_4b");

	
  cout << "END   Initialization" << endl;
  return true;
}

// -----------------------------------------------------------------------------
// Finalize
// function called one time at the end of the analysis
// -----------------------------------------------------------------------------
void SUSY_2019_04::Finalize(const SampleFormat& summary, const std::vector<SampleFormat>& files)
{
  cout << "BEGIN Finalization" << endl;
  write_data(nn_6j_output,"nn_6j_output_jet_score_sorted.csv");    
  // saving histos
  cout << "END   Finalization" << endl;
}

// -----------------------------------------------------------------------------
// Execute
// function called each time one event is read
// -----------------------------------------------------------------------------
bool SUSY_2019_04::Execute(SampleFormat& sample, const EventFormat& event)
{

  // Event weight
  MAdouble64 EvWeight;
  if(Configuration().IsNoEventWeight()) EvWeight=1.;
  else if(event.mc()->weight()!=0.) EvWeight=event.mc()->weight();
  else { return false;}
  Manager()->InitializeForNewEvent(EvWeight);

  // Empty event
  if (event.rec()==0) {return true;}

  std::vector<int> jets_20_score, jets_score, jets_score_sort, jets_score_non_b, jets_score_b;
  std::vector<const RecJetFormat*>    BaseJets, BaseJets_sort, BaseJets_b, BaseJets_non_b, SignalBJets20, SignalJets20;
  std::vector<const RecJetFormat*>    SignalBJets40, SignalJets40;
  std::vector<const RecJetFormat*>    SignalBJets60, SignalJets60;
  std::vector<const RecJetFormat*>    SignalBJets80, SignalJets80;
  std::vector<const RecJetFormat*>    SignalBJets100, SignalJets100;
  std::vector<const RecLeptonFormat*> preBaseMuons, preBaseElectrons;
  std::vector<const RecLeptonFormat*> BaseMuons, BaseElectrons;
  std::vector<const RecLeptonFormat*> SignalMuons, SignalElectrons;


  // Electron with pT > 10 GeV & eta < 2.47
  for (MAuint32 ie=0; ie<event.rec()->electrons().size(); ie++)
  {
    const RecLeptonFormat *CurrentElectron = &(event.rec()->electrons()[ie]);
    if ( CurrentElectron->pt()>10 && CurrentElectron->abseta() < 2.47)
    {
        preBaseElectrons.push_back(CurrentElectron);
    }
  }
  DEBUG << "    * Reconstructed Electron..." << endmsg;

  // Muons with pT > 10 GeV & eta < 2.5
  for(MAuint32 im=0; im<event.rec()->muons().size(); im++)
  {
    const RecLeptonFormat *CurrentMuon = &(event.rec()->muons()[im]);
    if( CurrentMuon->pt()>10. && CurrentMuon->abseta() < 2.5)
    {
        preBaseMuons.push_back(CurrentMuon);
    }
  }
  DEBUG << "    * Reconstructed Muon..." << endmsg;

//  number of Leptons 
  MAint32  npreBaseLeptons = preBaseMuons.size() + preBaseElectrons.size();


  // Jets
  for(MAuint32 ij=0; ij<event.rec()->jets().size(); ij++)
  {
    const RecJetFormat *CurrentJet = &(event.rec()->jets()[ij]);
    if ( CurrentJet->pt() > 20.0 && CurrentJet->abseta() < 4.5)
    {
      //cout<<" btag function"<<endl;

      int score = jet_btag_category(CurrentJet);

      if (CurrentJet->btag()){
        BaseJets_b.push_back(CurrentJet); //  no weight a.t.m for overlapp 
        jets_score_b.push_back(score);
        } //  no weight a.t.m for overlapp 
      else {
        BaseJets_non_b.push_back(CurrentJet);
        jets_score_non_b.push_back(score);
        }
      //cout <<"b size" <<jets_score_b.size() << endl;
      //cout <<"last " << jets_score_b.back() << endl;
    }
  }
  //cout <<"b size" <<jets_score_b.size() << endl;
  //cout <<"non b size" <<jets_score_non_b.size() << endl;

  DEBUG << "    * Reconstructed Jets..." << endmsg;

  // overlap removal
  preBaseElectrons = Removal(preBaseElectrons, preBaseMuons,  0.01); 
  BaseJets_non_b   = Removal( BaseJets_non_b, preBaseElectrons, 0.2);
  BaseJets_non_b   = Removal( BaseJets_non_b,   preBaseMuons,  0.4); //muon.Pt()/jet.Pt()>0.5))
  int removed_jets = jets_score_non_b.size() - BaseJets_non_b.size(); // after overlap removal
  if (removed_jets != 0){
    int i = 0;
    while (i<removed_jets){
      jets_score_non_b.pop_back(); /// if non_b jets are removed we must remove them from the score vector
      i++;                          // index of jet does not matter all have the same score
    }
  }

  BaseJets = BaseJets_b;
  jets_score = jets_score_b; 
  BaseJets.insert(BaseJets.end(), BaseJets_non_b.begin(), BaseJets_non_b.end()); 
  jets_score.insert(jets_score.end(), jets_score_non_b.begin(), jets_score_non_b.end()); 
  BaseElectrons    = Removal_last( preBaseElectrons,    BaseJets);
  BaseMuons        = Removal_last( preBaseMuons,        BaseJets);

  BaseJets_sort=BaseJets;
  SORTER->sort(BaseJets_sort); // score vector not sorted 
  MAuint32 n_bj = BaseJets.size();

  for(MAuint32 i=0; i<n_bj; i++){
    for(MAuint32 j=0; j<n_bj; j++){
      if(BaseJets[j]->pt() == BaseJets_sort[i]->pt()){
        jets_score_sort.push_back(jets_score[j]);
        break;
      }
    }
  }

  if (jets_score_sort.size()!=n_bj) {
    cout << "ERROR SORTING B-SCORE" << endl;
    return true;
  }

  //cout << "before sortng" << endl;
 //for(MAuint32 i=0; i<jets_score.size(); i++){ cout << jets_score[i] << endl;}
  //cout << "after sortng" << endl;
  //for(MAuint32 i=0; i<jets_score_sort.size(); i++){ cout << jets_score_sort[i] << endl;}

  // signal objects 

  // Select signal electrons
  for (MAuint32 i=0; i<BaseElectrons.size(); i++)
  {
    if ( BaseElectrons[i]->pt() > 15.0 && BaseElectrons[i]->abseta() < 2.47)
    {
      SignalElectrons.push_back(BaseElectrons[i]);
    }
  }

 // Select signal muons
  for (MAuint32 i=0; i<BaseMuons.size(); i++)
  {
    if ( BaseMuons[i]->pt() > 15.0 && BaseMuons[i]->abseta() < 2.4)
    {
      SignalMuons.push_back(BaseMuons[i]);
    }
  }


 // Vector for both leptons
  std::vector<const MALorentzVector*> SignalLeptons; 
  int n_e = SignalElectrons.size();
  int n_m = SignalMuons.size();
  for (int i=0; i<(n_e+n_m); i++ ){
    if (i<n_e){
      SignalLeptons.push_back(&SignalElectrons[i]->momentum());
    }
    else{
      SignalLeptons.push_back(&SignalMuons[i-n_e]->momentum());
    }
  }


  //SORTER->sort(SignalLeptons);
  MAint32  nSignalLeptons = SignalLeptons.size(); //ne + nm

  MAdouble64 pt_max = -99.0;
  MAuint32 i_leading;
  for (MAuint32 i=0; i<nSignalLeptons;i++){
    if (SignalLeptons[i]->Pt() > pt_max){
      pt_max = SignalLeptons[i]->Pt();
      i_leading=i;
    }
  }


 //Signal jet and b-jet for 20 GeV cut

 for (MAuint32 i=0; i<n_bj; i++)
  {
    if ( BaseJets_sort[i]->pt() > 20.0 && BaseJets_sort[i]->abseta() < 2.5)
    {
      SignalJets20.push_back(BaseJets_sort[i]);
      jets_20_score.push_back(jets_score_sort[i]);
      if (jets_score_sort[i]>4){
          SignalBJets20.push_back(BaseJets_sort[i]);
      }
    }
  }

 //Signal jet and b-jet for 40 GeV cut
 
 for (MAuint32 i=0; i<SignalJets20.size(); i++)
  {
    if ( SignalJets20[i]->pt() > 40.0 && SignalJets20[i]->abseta() < 2.5)
    {
      SignalJets40.push_back(SignalJets20[i]);
      if (jets_20_score[i]>4){
     // if ( SignalJets20[i]->btag() && random_bool_with_prob(0.7)){
          SignalBJets40.push_back(SignalJets20[i]);
      }
    }
  }

  //Signal jet and b-jet for 60 GeV cut
 
 for (MAuint32 i=0; i<SignalJets20.size(); i++)
  {
    if ( SignalJets20[i]->pt() > 60.0 && SignalJets20[i]->abseta() < 2.5)
    {
      SignalJets60.push_back(SignalJets20[i]);
      if (jets_20_score[i]>4){
          SignalBJets60.push_back(SignalJets20[i]);
      }
    }
  }

  //Signal jet and b-jet for 80 GeV cut
 
 for (MAuint32 i=0; i<SignalJets20.size(); i++)
  {
    if ( SignalJets20[i]->pt() > 80.0 && SignalJets20[i]->abseta() < 2.5)
    {
      SignalJets80.push_back(SignalJets20[i]);
      if (jets_20_score[i]>4){
          SignalBJets80.push_back(SignalJets20[i]);
      }
    }
  }
  //Signal jet and b-jet for 100 GeV cut
 
 for (MAuint32 i=0; i<SignalJets20.size(); i++)
  {
    if ( SignalJets20[i]->pt() > 100.0 && SignalJets20[i]->abseta() < 2.5)
    {
      SignalJets100.push_back(SignalJets20[i]);
      if (jets_20_score[i]>4){
          SignalBJets100.push_back(SignalJets20[i]);
      }
    }
  }

  DEBUG << "    * Event reconstructed properly..." << endmsg;


  ////////////////////////
  // Preselection
	// at least one lepton, leading lepton with at least 27 GeV
  if (nSignalLeptons < 1) {return true;}
  Manager()->ApplyCut(1, "presel nl>1");


	if (nSignalLeptons < 1 || SignalLeptons[i_leading]->Pt() < 27) {return true;}

  Manager()->ApplyCut(1, "presel nl>1 ; lead pt > 27 GeV");

  // at least 4 jets with pT > 20 GeV
	if (SignalJets20.size() < 4) {return true;}

  Manager()->ApplyCut(1,"presel njets > 4 ; > 20 GeV");


  //==================//
  //==== Cut Flow ====//
  //==================//

  DEBUG << "    * Starting cut-flow..." << endmsg;


  bool isSameSignChannel;
  
  if(n_e==0){
    isSameSignChannel =    (nSignalLeptons == 2)
                   && (SignalMuons[0]->charge() == SignalMuons[1]->charge())
                   && (npreBaseLeptons == 2);
  } else if (n_m==0){
    isSameSignChannel =    (nSignalLeptons == 2)
                   && (SignalElectrons[0]->charge() == SignalElectrons[1]->charge())
                   && (npreBaseLeptons == 2);
  }else{
    isSameSignChannel =    (nSignalLeptons == 2)
	                  && (SignalElectrons[0]->charge() == SignalMuons[0]->charge())
	                  && (npreBaseLeptons == 2);  
  }


const bool isOneLeptonChannel = !isSameSignChannel;
Manager()->ApplyCut(isOneLeptonChannel, "one_lepton_cut");

Manager()->ApplyCut(SignalJets20.size() >=15,"15 jets > 20 Gev");
Manager()->ApplyCut(SignalJets40.size() >=12,"12 jets > 40 Gev");
Manager()->ApplyCut(SignalJets60.size() >=12,"12 jets > 60 Gev");
Manager()->ApplyCut(SignalJets80.size() >=10,"10 jets > 80 Gev");
Manager()->ApplyCut(SignalJets100.size() >=8,"8 jets > 100 Gev");




////////////////////////////
	// Jet counting analysis

	if (isOneLeptonChannel) {	

     // Jets 20 
     Manager()->ApplyCut(SignalJets20.size() >= 15 && SignalBJets20.size() == 0, "1l_15j20_0b_cut");
     Manager()->ApplyCut(SignalJets20.size() >= 15 && SignalBJets20.size() >= 3, "1l_15j20_3b_cut");

     //Jets 40
     Manager()->ApplyCut(SignalJets40.size() >= 12 && SignalBJets40.size() == 0, "1l_12j40_0b_cut");
     Manager()->ApplyCut(SignalJets40.size() >= 12 && SignalBJets40.size() >= 3, "1l_12j40_3b_cut");

     //Jets 60
     Manager()->ApplyCut(SignalJets60.size() >= 12 && SignalBJets60.size() == 0, "1l_12j60_0b_cut");
     Manager()->ApplyCut(SignalJets60.size() >= 12 && SignalBJets60.size() >= 3, "1l_12j60_3b_cut");

     //Jets 80
     Manager()->ApplyCut(SignalJets80.size() >= 10 && SignalBJets80.size() == 0, "1l_10j80_0b_cut");
     Manager()->ApplyCut(SignalJets80.size() >= 10 && SignalBJets80.size() >= 3, "1l_10j80_3b_cut");

     //Jets 100
     Manager()->ApplyCut(SignalJets100.size() >= 8 && SignalBJets100.size() == 0, "1l_8j100_0b_cut");
     Manager()->ApplyCut(SignalJets100.size() >= 8 && SignalBJets100.size() >= 3, "1l_8j100_3b_cut");
   }

  if (isSameSignChannel){
    Manager()->ApplyCut(isSameSignChannel, "sameSignChannel");

    Manager()->ApplyCut(SignalJets20.size() >= 10 && SignalBJets20.size() == 0, "ss_10j20_0b_cut");
    Manager()->ApplyCut(SignalJets20.size() >= 10 && SignalBJets20.size() == 3, "ss_10j20_3b_cut");

    Manager()->ApplyCut(SignalJets40.size() >= 8 && SignalBJets40.size() == 0, "ss_8j40_0b_cut");
    Manager()->ApplyCut(SignalJets40.size() >= 8 && SignalBJets40.size() == 3, "ss_8j40_3b_cut");

    Manager()->ApplyCut(SignalJets60.size() >= 7 && SignalBJets60.size() == 0, "ss_7j60_0b_cut");
    Manager()->ApplyCut(SignalJets60.size() >= 7 && SignalBJets60.size() == 3, "ss_7j60_3b_cut");
  
    Manager()->ApplyCut(SignalJets80.size() >= 6 && SignalBJets80.size() == 0, "ss_6j80_0b_cut");
    Manager()->ApplyCut(SignalJets80.size() >= 6 && SignalBJets80.size() == 3, "ss_6j80_3b_cut");

    Manager()->ApplyCut(SignalJets100.size() >= 6 && SignalBJets100.size() == 0, "ss_6j100_0b_cut");
    Manager()->ApplyCut(SignalJets100.size() >= 6 && SignalBJets100.size() == 3, "ss_6j100_3b_cut");

  }

////////////////////////////
	// Shape analysis

	if (isOneLeptonChannel && SignalJets20.size() >= 4 && SignalJets20.size() <= 8 && SignalBJets20.size() >= 1) { 
   //cout << "shape analysis " << std::endl;
   // cout << "N_jets = " << SignalJets20.size() << std::endl;

		// prepare NN inputs
		const float Escale = 100; // GeV

    MALorentzVector pTmiss_0 = (event.rec()->MET().momentum());
    MALorentzVector* pTmiss = &pTmiss_0;
    MAdouble64 met         = pTmiss->Pt();
    MAdouble64 met_phi     = pTmiss->Phi();

		std::vector<float> nn_inputs;
    nn_inputs.reserve(65);
    nn_inputs.push_back(SignalJets20.size()); // n_jet
    nn_inputs.push_back(bjet_category(SignalBJets20.size())); // n_b_category
    nn_inputs.push_back(SignalBJets20.size()); // n_bjet
    nn_inputs.push_back(calc_three_jet_max_pt_mass(SignalJets20) / Escale); // three_jet_maxpt_mass
  	nn_inputs.push_back(calc_ht(SignalJets20) / Escale); // ht_jet
		nn_inputs.push_back(calc_ht(SignalBJets20) / Escale); // ht_bjet
		nn_inputs.push_back(calc_three_jet_max_pt_mass(SignalJets20, SignalLeptons[i_leading], pTmiss) / Escale); // three_jet_lep_met_mass_max_pt
		nn_inputs.push_back(min_dr_lep_jet(SignalLeptons, SignalJets20)); // minDeltaRlj lep ins Lorentz
		nn_inputs.push_back(calc_minmax_mass(SignalJets20) / Escale); // minmax_mass
		nn_inputs.push_back(met / Escale); // met
		nn_inputs.push_back(met_phi); // met_phi

    // jet_pt 0-9
		nn_inputs.push_back(SignalJets20.size() > 1 ? SignalJets20[0]->pt() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 0 ? SignalJets20[1]->pt() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 2 ? SignalJets20[2]->pt() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 3 ? SignalJets20[3]->pt() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 4 ? SignalJets20[4]->pt() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 5 ? SignalJets20[5]->pt() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 6 ? SignalJets20[6]->pt() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 7 ? SignalJets20[7]->pt() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 8 ? SignalJets20[8]->pt() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 9 ? SignalJets20[9]->pt() / Escale : 1e-7);

		// jet_eta 0-9
		nn_inputs.push_back(SignalJets20.size() > 0 ? SignalJets20[0]->eta() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 1 ? SignalJets20[1]->eta() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 2 ? SignalJets20[2]->eta() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 3 ? SignalJets20[3]->eta() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 4 ? SignalJets20[4]->eta() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 5 ? SignalJets20[5]->eta() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 6 ? SignalJets20[6]->eta() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 7 ? SignalJets20[7]->eta() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 8 ? SignalJets20[8]->eta() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 9 ? SignalJets20[9]->eta() : -5.0);

		// jet_phi 0-9
		nn_inputs.push_back(SignalJets20.size() > 0 ? SignalJets20[0]->phi() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 1 ? SignalJets20[1]->phi() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 2 ? SignalJets20[2]->phi() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 3 ? SignalJets20[3]->phi() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 4 ? SignalJets20[4]->phi() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 5 ? SignalJets20[5]->phi() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 6 ? SignalJets20[6]->phi() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 7 ? SignalJets20[7]->phi() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 8 ? SignalJets20[8]->phi() : -5.0);
		nn_inputs.push_back(SignalJets20.size() > 9 ? SignalJets20[9]->phi() : -5.0);

  // jet_e 0-9
		nn_inputs.push_back(SignalJets20.size() > 0 ? SignalJets20[0]->e() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 1 ? SignalJets20[1]->e() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 2 ? SignalJets20[2]->e() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 3 ? SignalJets20[3]->e() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 4 ? SignalJets20[4]->e() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 5 ? SignalJets20[5]->e() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 6 ? SignalJets20[6]->e() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 7 ? SignalJets20[7]->e() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 8 ? SignalJets20[8]->e() / Escale : 1e-7);
		nn_inputs.push_back(SignalJets20.size() > 9 ? SignalJets20[9]->e() / Escale : 1e-7);

		// jet_btag_bin 0-9
		nn_inputs.push_back(SignalJets20.size() > 0 ? jets_20_score[0] : 0);
		nn_inputs.push_back(SignalJets20.size() > 1 ? jets_20_score[1] : 0);
		nn_inputs.push_back(SignalJets20.size() > 2 ? jets_20_score[2] : 0);
		nn_inputs.push_back(SignalJets20.size() > 3 ? jets_20_score[3] : 0);
		nn_inputs.push_back(SignalJets20.size() > 4 ? jets_20_score[4] : 0);
		nn_inputs.push_back(SignalJets20.size() > 5 ? jets_20_score[5] : 0);
		nn_inputs.push_back(SignalJets20.size() > 6 ? jets_20_score[6] : 0);
		nn_inputs.push_back(SignalJets20.size() > 7 ? jets_20_score[7] : 0);
		nn_inputs.push_back(SignalJets20.size() > 8 ? jets_20_score[8] : 0);
		nn_inputs.push_back(SignalJets20.size() > 9 ? jets_20_score[9] : 0);

		nn_inputs.push_back(SignalLeptons[i_leading]->Pt() / Escale);
		nn_inputs.push_back(SignalLeptons[i_leading]->Eta());
		nn_inputs.push_back(SignalLeptons[i_leading]->Phi());
		nn_inputs.push_back(SignalLeptons[i_leading]->E() / Escale);


  	// now select which NN to use, according to jet multiplicity

    float nn_value ; 
    cout << "nn_value = " << nn_value << std::endl;

    Manager()->ApplyCut(SignalBJets20.size() >= 4 ,"B-jets >= 4 (>20 GeV)");

		switch (SignalJets20.size()) {
		case 4:
		  nn_value = run_ONNX(nn_path_4j, nn_inputs);
      //cout << "in case 4 " << std::endl;
      //cout << "nn_value = " << nn_value << std::endl;
      Manager()->FillHisto("4jets",   nn_value);
      Manager()->ApplyCut(SignalBJets20.size() >= 4 && nn_value > 0.73, "1l_shape_4j_4b_cut");
		  break;
		case 5:
		  nn_value = run_ONNX(nn_path_5j, nn_inputs);
      //cout << "in case 5 " << std::endl;
      //cout << "nn_value = " << nn_value << std::endl;
      Manager()->FillHisto("5jets",   nn_value);
      Manager()->ApplyCut(SignalBJets20.size() >= 4 && nn_value > 0.76, "1l_shape_5j_4b_cut");
		  break;
		case 6:
		  nn_value = run_ONNX(nn_path_6j, nn_inputs);
      //cout << "in case 6 " << std::endl;
      //cout << "nn_value = " << nn_value << std::endl;
      nn_6j_output.push_back(nn_value); 
      Manager()->FillHisto("6jets",   nn_value);
      Manager()->ApplyCut(SignalBJets20.size() >= 4 && nn_value > 0.77, "1l_shape_6j_4b_cut");
		  break;
		case 7:
		  nn_value = run_ONNX(nn_path_7j, nn_inputs);
      //////cout << "in case 7 " << std::endl;
      //////cout << "nn_value = " << nn_value << std::endl;
      Manager()->FillHisto("7jets",   nn_value);
      Manager()->ApplyCut(SignalBJets20.size() >= 4 && nn_value > 0.72, "1l_shape_7j_4b_cut");
		  break;
		case 8:
		  nn_value = run_ONNX(nn_path_8j, nn_inputs);
      //cout << "in case 8 " << std::endl;
      //cout << "nn_value = " << nn_value << std::endl;
      Manager()->FillHisto("8jets",   nn_value);
      Manager()->ApplyCut(SignalBJets20.size() >= 4 && nn_value > 0.73, "1l_shape_8j_4b_cut");
		  break;
		default:
			assert(false);
		}
    }
  i_event = i_event +1;
  if (i_event-i_event_old > 100){
    cout << "Event n " << i_event << endl;
    i_event_old = i_event;
  }
  return true;
}